﻿using AoCHelper;

namespace AdventOfCode
{
    public class Day_01 : BaseDay
    {
        private readonly string[] _input;

        public Day_01()
        {
            _input = File.ReadAllLines(InputFilePath);
        }

        public override ValueTask<string> Solve_1()
        {
            int sum = 0;
            for (int i = 0; i < _input.Length; i++)
            {
                sum += GetFuelRequiredBase(int.Parse(_input[i]));
            }
            return new(sum.ToString());
        }

        public override ValueTask<string> Solve_2()
        {
            int sum = 0;
            for (int i = 0; i < _input.Length; i++)
            {
                sum += GetFuelRequiredTotal(int.Parse(_input[i]));
            }
            return new(sum.ToString());
        }

        int GetFuelRequiredBase(int mass)
        {
            return Math.Max(mass / 3 - 2, 0);
        }

        int GetFuelRequiredTotal(int mass)
        {
            if (mass < 3)
            {
                return 0;
            }
            else
            {
                int baseFuel = GetFuelRequiredBase(mass);
                return baseFuel + GetFuelRequiredTotal(baseFuel);
            }
        }
    }
}
